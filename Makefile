.PHONY: format
format:
	go run "github.com/golangci/golangci-lint/cmd/golangci-lint@latest" run ./...

.PHONY: compile
compile:
	mkdir -p bin
	go build -o bin/projectsIndex "$(CURDIR)/cmd/projectsIndex"
	go build -o bin/projectGraph "$(CURDIR)/cmd/projectGraph"

.PHONY: indexing
indexing:
	bin/projectsIndex \
		-templates-path="$(CURDIR)/templates" \
		-output-path="$(CURDIR)/public" \
		-projects-path="$(CURDIR)/public/project"

define clone
	mkdir -p "$(CURDIR)/tmp"
	git clone --depth 1 $(1) $(CURDIR)/tmp/$(2)
endef

.PHONY: container-registry
container-registry:
	$(call clone,"https://gitlab.com/gitlab-org/container-registry","container-registry")
	bin/projectGraph \
		-package="./cmd/registry" \
		-package-path="$(CURDIR)/tmp/container-registry" \
		-prefix-filter="github.com/docker/distribution" \
		-output-path="$(CURDIR)/public/project/container-registry" \
		-templates-path="$(CURDIR)/templates"

.PHONY: gitaly
gitaly:
	$(call clone,"https://gitlab.com/gitlab-org/gitaly","gitaly")
	bin/projectGraph \
		-package="./cmd/gitaly" \
		-package-path="$(CURDIR)/tmp/gitaly" \
		-prefix-filter="gitlab.com/gitlab-org/gitaly" \
		-output-path="$(CURDIR)/public/project/gitaly" \
		-templates-path="$(CURDIR)/templates"

.PHONY: gitlab-pages
gitlab-pages:
	$(call clone,"https://gitlab.com/gitlab-org/gitlab-pages","gitlab-pages")
	bin/projectGraph \
		-package="gitlab.com/gitlab-org/gitlab-pages" \
		-package-path="$(CURDIR)/tmp/gitlab-pages" \
		-prefix-filter="gitlab.com/gitlab-org/gitlab-pages" \
		-output-path="$(CURDIR)/public/project/gitlab-pages" \
		-templates-path="$(CURDIR)/templates"

.PHONY: gitlab-runner
gitlab-runner:
	$(call clone,"https://gitlab.com/gitlab-org/gitlab-runner","gitlab-runner")
	bin/projectGraph \
		-package="gitlab.com/gitlab-org/gitlab-runner" \
		-package-path="$(CURDIR)/tmp/gitlab-runner" \
		-prefix-filter="gitlab.com/gitlab-org/gitlab-runner" \
		-output-path="$(CURDIR)/public/project/gitlab-runner" \
		-templates-path="$(CURDIR)/templates"

.PHONY: gitlab-shell-clone
gitlab-shell-clone:
	$(call clone,"https://gitlab.com/gitlab-org/gitlab-shell","gitlab-shell")

.PHONY: gitlab-shell
gitlab-shell: gitlab-shell-clone
	bin/projectGraph \
		-package="./cmd/gitlab-shell" \
		-package-path="$(CURDIR)/tmp/gitlab-shell" \
		-prefix-filter="gitlab.com/gitlab-org/gitlab-shell" \
		-output-path="$(CURDIR)/public/project/gitlab-shell" \
		-templates-path="$(CURDIR)/templates"

.PHONY: gitlab-sshd
gitlab-sshd: gitlab-shell-clone
	bin/projectGraph \
		-package="./cmd/gitlab-sshd" \
		-package-path="$(CURDIR)/tmp/gitlab-shell" \
		-prefix-filter="gitlab.com/gitlab-org/gitlab-shell" \
		-output-path="$(CURDIR)/public/project/gitlab-sshd" \
		-templates-path="$(CURDIR)/templates"

.PHONY: glab
glab:
	$(call clone,"https://gitlab.com/gitlab-org/cli","glab")
	bin/projectGraph \
		-package="./cmd/glab" \
		-package-path="$(CURDIR)/tmp/glab" \
		-prefix-filter="gitlab.com/gitlab-org/cli" \
		-output-path="$(CURDIR)/public/project/glab" \
		-templates-path="$(CURDIR)/templates"

.PHONY: release-cli
release-cli:
	$(call clone,"https://gitlab.com/gitlab-org/release-cli","release-cli")
	bin/projectGraph \
		-package="./cmd/release-cli" \
		-package-path="$(CURDIR)/tmp/release-cli" \
		-prefix-filter="gitlab.com/gitlab-org/release-cli" \
		-output-path="$(CURDIR)/public/project/release-cli" \
		-templates-path="$(CURDIR)/templates"

.PHONY: workhorse
workhorse:
	$(call clone,"https://gitlab.com/gitlab-org/gitlab","gitlab")
	bin/projectGraph \
		-package="gitlab.com/gitlab-org/gitlab/workhorse" \
		-package-path="$(CURDIR)/tmp/gitlab/workhorse" \
		-prefix-filter="gitlab.com/gitlab-org/gitlab/workhorse" \
		-output-path="$(CURDIR)/public/project/workhorse" \
		-templates-path="$(CURDIR)/templates"

-include Makefile.dev
