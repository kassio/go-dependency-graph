# Go dependency graph for some gitlab projects

- [https://kassio.gitlab.io/go-dependency-graph/](https://kassio.gitlab.io/go-dependency-graph/)

## How it works

This project has two different commands:

1. `projectsIndex` which creates the main page for the site (`public/index.html`) with the list of projects,
    based on the `templates/index.html.tpl`.
1. `projectGraph` which creates per project assets:
    - `public/project/project_name/index.html`: the webpage that renders the graph with `d3-graphviz`,
       based on `templates/project.html.tpl`
    - `public/project/project_name/graph.dot`: the graphviz graph
       based on `templates/graph.dot.tpl`
    - `public/project/project_name/relations.json`: a relation map to highlight paths on click

## Add a new project

You can check how the other projects are doing it. Basically you have to edit two files:

1. **Makefile** Add a Makefile target for the new project:

    ```makefile
    .PHONY: new_project_name
    new_project_name:
      $(call clone,"<<git url for the project>>","<<folder name to clone>>")
      bin/projectGraph \
        -package="<<project main package name or path>>" \
        -package-path="$(CURDIR)/tmp/<<folder name where it was cloned>>" \
        -prefix-filter="<<only show imports from packages with this prefix>>" \
        -output-path="$(CURDIR)/public/project/<<folder to save generated assets>>" \
        -templates-path="$(CURDIR)/templates"
    ```

1. **.gitlab-ci.yml** Add a CI job in the `generate` stage

    ```yaml
    gitlab-pages:
      << : *generate-stage
      script:
        - make new_project_name
    ```

1. **.gitlab-ci.yml** Add the new job to the `pages` dependencies list

    ```yaml
    pages:
      stage: pages
      dependencies:
        - compile
        - gitaly
        - new_project_name
      script:
        - make indexing
      artifacts:
        paths:
          - public
      ```
