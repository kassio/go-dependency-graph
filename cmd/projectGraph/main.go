package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/kassio/go-dependency-graph/internal/generator"
	"gitlab.com/kassio/go-dependency-graph/internal/importMap"
)

func main() {
	currentPath, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	var pkgName string
	flag.StringVar(&pkgName, "package", "", "package to process")
	var pkgPath string
	flag.StringVar(&pkgPath, "package-path", "", "package to process")
	var prefix string
	flag.StringVar(&prefix,
		"prefix-filter",
		"",
		"prefix filter - only show imports for packages with this prefix ")
	var templatesPath string
	flag.StringVar(&templatesPath,
		"templates-path",
		"",
		"templates folder (graph.dot.tpl, project.html.tpl)")
	var outputPath string
	flag.StringVar(&outputPath,
		"output-path",
		"",
		"where the assets will be written")
	flag.Parse()

	err = os.Chdir(pkgPath)
	if err != nil {
		fmt.Println("failed to change to:", err)
		return
	}

	impMap, err := importMap.Load(pkgName, pkgPath, prefix)
	if err != nil {
		fmt.Println("failed to change to:", err)
		return
	}
	generator.Generate(impMap, pkgName, templatesPath, outputPath)

	err = os.Chdir(currentPath)
	if err != nil {
		fmt.Println("failed to change to:", err)
	}
}
