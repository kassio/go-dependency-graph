<!DOCTYPE html>
<html>
  <head>
    <title>Gitlab go projects: dependencies graph</title>
  </head>

  <body>
    <header>
      <h1>Gitlab go projects: dependencies graph</h1>
    </header>

    <main>
      <ul>
        {{- range $index, $project := .Projects }}
        <li>
          <a href="{{ $project.Path }}">{{ $project.Name }}</a>
        </li>
        {{- end }}
      </ul>
    </main>
  </body>
</html>
