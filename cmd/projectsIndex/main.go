package main

import (
	_ "embed"
	"flag"
	"fmt"
	"html/template"
	"io/fs"
	"os"
	"path/filepath"
)

//go:embed index.html.tpl
var tplIndex string

const indexFilename = "index.html"

type project struct {
	Path string
	Name string
}

type projects struct {
	Projects []project
}

func main() {
	var projectsPath string
	flag.StringVar(&projectsPath,
		"projects-path",
		"",
		"where the assets will be written")
	var templatesPath string
	flag.StringVar(&templatesPath,
		"templates-path",
		"",
		"templates folder (graph.dot.tpl, project.html.tpl)")
	var outputPath string
	flag.StringVar(&outputPath,
		"output-path",
		"",
		"where the assets will be written")
	flag.Parse()

	tpl, err := template.New("index").Parse(tplIndex)
	if err != nil {
		panic(err)
	}

	filePath := filepath.Join(outputPath, indexFilename)
	file, err := os.Create(filePath)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	projList := listProjects(outputPath, projectsPath)

	err = tpl.Execute(file, projList)
	if err != nil {
		panic(err)
	}

	fmt.Printf("created: %q\n", filePath)
}

func listProjects(outputPath, projectsPath string) projects {
	projList := []project{}
	err := filepath.Walk(projectsPath,
		func(path string, info fs.FileInfo, _ error) error {
			if !info.IsDir() || path == projectsPath {
				return nil
			}

			name := filepath.Base(path)
			rpath, err := filepath.Rel(outputPath, path)
			if err != nil {
				return nil
			}

			projList = append(projList, project{
				Name: name,
				Path: rpath,
			})

			return nil
		})

	if err != nil {
		fmt.Printf("error while listing projects %v\n", err)
	}

	return projects{Projects: projList}

}
