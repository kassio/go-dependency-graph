package generator

import (
	_ "embed"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"text/template"

	"gitlab.com/kassio/go-dependency-graph/internal/pkg"
)

type dependecyGraph struct {
	Name          string
	Relations     map[string][]pkg.Package
	templatesPath string
	outputPath    string
}

type Relation struct {
	Descendants []string `json:"descendants"`
	Ascendants  []string `json:"ascendants"`
}

type RelationsMap map[string]Relation

//go:embed project.html.tpl
var projectIndexTpl string

//go:embed graph.dot.tpl
var graphvizTpl string

const (
	projectIndexFile = "index.html"
	graphvizFile     = "graph.dot"
	relationsFile    = "relations.json"
)

func Generate(impMap map[string][]pkg.Package, pkgName, templatesPath, outputPath string) {
	depGraph := dependecyGraph{
		Name:          filepath.Base(pkgName),
		Relations:     impMap,
		templatesPath: templatesPath,
		outputPath:    outputPath,
	}

	if err := os.MkdirAll(depGraph.outputPath, 0755); err != nil {
		panic(err)
	}

	generateRelationsJSONFile(depGraph)
	generateFromTemplate(projectIndexTpl, projectIndexFile, depGraph)
	generateFromTemplate(graphvizTpl, graphvizFile, depGraph)
}

func generateRelationsJSONFile(depGraph dependecyGraph) {
	relations := RelationsMap{}

	for pkg, pkgs := range depGraph.Relations {
		descendants := make([]string, len(pkgs))

		for i, imp := range pkgs {
			descendants[i] = imp.ImportPath
			impRelations := relations[imp.ImportPath]

			if impRelations.Ascendants == nil {
				impRelations.Ascendants = []string{}
			}

			impRelations.Ascendants = append(impRelations.Ascendants, pkg)
			relations[imp.ImportPath] = impRelations
		}

		pkgRelations := relations[pkg]
		pkgRelations.Descendants = descendants
		relations[pkg] = pkgRelations
	}

	j, err := json.Marshal(relations)
	if err != nil {
		panic(err)
	}

	filePath := filepath.Join(depGraph.outputPath, relationsFile)
	file, err := os.Create(filePath)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	if _, err := file.Write(j); err != nil {
		panic(err)
	}

	fmt.Printf("created: %q\n", filePath)
}

func generateFromTemplate(tplFile, outFilename string, depGraph dependecyGraph) {
	tpl, err := template.New("tpl").Parse(tplFile)
	if err != nil {
		panic(err)
	}

	filePath := filepath.Join(depGraph.outputPath, outFilename)
	file, err := os.Create(filePath)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	err = tpl.Execute(file, depGraph)
	if err != nil {
		panic(err)
	}

	fmt.Printf("created: %q\n", filePath)
}
