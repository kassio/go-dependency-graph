digraph "{{.Name}}" {
  fontname="Helvetica,Arial,sans-serif"
  nodesep=0.1
  rankdir="LR"
  ranksep=1

  edge [
    arrowsize="0.5"
  ]

  node [
    fillcolor="#00EEEE"
    fontname="Helvetica,Arial,sans-serif"
    fontsize=10
    height=0.4
    pencolor="#00000044"
    shape=box
    style="rounded,filled"
  ]

  {{ range $package, $imports := .Relations }}
  {{- range $index, $import := $imports }}
  {{- if not $import.HasPrefix }}
  "{{ $import.ImportPath }}" [ fillcolor="#008B8B" ];
  {{- end }}
  "{{ $package -}}" -> "{{ $import.ImportPath }}";
  {{- end }}
  {{- end }}
}
