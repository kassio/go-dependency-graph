<!DOCTYPE html>
<html>
  <head>
    <title>{{.Name}}</title>
    <script src="../../assets/d3.v5.min.js"></script>
    <script src="../../assets/index.min.js"></script>
    <script src="../../assets/d3-graphviz.js"></script>
    <script defer async src="../../assets/app.js"></script>
  </head>

  <body>
    <a href="../../">Project list</a>
    <main>
      <h1>{{.Name}}</h1>
      <div id="graph"></div>
    </main>
  </body>
</html>
