package importMap

import (
	"fmt"
	"path/filepath"

	"gitlab.com/kassio/go-dependency-graph/internal/pkg"
)

func Load(pkgName, pkgPath, prefix string) (map[string][]pkg.Package, error) {
	p, err := pkg.Find(pkgName, pkgPath, prefix)
	if err != nil {
		return nil, err
	}

	impMap := map[string][]pkg.Package{}

	load(impMap, p, filepath.Base(pkgName), prefix)

	return impMap, nil
}

func load(impMap map[string][]pkg.Package, p pkg.Package, pkgName, prefix string) {
	fmt.Println(">", p.ImportPath)

	for _, imp := range p.Imports {
		importedPkg, err := pkg.Find(imp, p.Dir, prefix)
		if err != nil || importedPkg.Stdlib {
			continue
		}

		impMap[pkgName] = append(impMap[pkgName], importedPkg)

		// already mapped or
		// doesn't match prefix filter
		if impMap[imp] != nil || !importedPkg.HasPrefix {
			continue
		}

		load(impMap, importedPkg, importedPkg.ImportPath, prefix)
	}
}
