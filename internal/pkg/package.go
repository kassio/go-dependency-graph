package pkg

import (
	"fmt"
	"go/build"
	"strings"
)

type Package struct {
	HasPrefix  bool
	Stdlib     bool
	*build.Package
}

func Find(pkgName, pkgPath, prefix string) (Package, error) {
	pkg, err := build.Import(pkgName, pkgPath, 0)
	if err != nil {
		return Package{}, fmt.Errorf("failed to find package: %w", err)
	}

	return Package{
		HasPrefix:  prefix != "" && strings.HasPrefix(pkg.ImportPath, prefix),
		Stdlib:     pkg.Goroot,
		Package:    pkg,
	}, nil
}
