(async function() {
  const dotFilePath = 'graph.dot'
  const relationFilePath = 'relations.json'
  const highlights = {
    selected: {
      stroke: '#EE0000',
      "stroke-width": '3px'
    },
    unselected: {
      stroke: '#000000',
      "stroke-width": '1px'
    },
  }

  let highlighted = { descendants: {}, ascendants: {} }

  const fetchFile = async (name) => {
    const uri = `${window.location.pathname}/${name}`.replaceAll('//', '/')

    return fetch(uri).catch(error =>
      console.log(`Failed to load '${name}' with: ${error}`)
    )
  }

  const dot =
    await fetchFile(dotFilePath).then(rsp => rsp.text())

  const relations =
    await fetchFile(relationFilePath).then(rsp => rsp.json())

  const selectByKey = (type, key) =>
    d3.selectAll(type).filter(n => n.key == key)

  const highlightPoints = (points, attributes) => {
    for (const [key, value] of Object.entries(attributes)) {
      points.attr(key, value)
    }
  }

  const getNodeTitle = (node) =>
    node.select('title').node().textContent

  const edgeRelation = (node, related, direction) => {
    let list = [node, related]

    if (direction == 'ascendants') {
      list = list.reverse()
    }

    return list.join('->')
  }

  const highlightTree = (node, direction) => {
    const nodeTitle = getNodeTitle(node)
    const nodePoints = node.selectAll('path,polygon,ellipse')
    const nodeRelations = relations[nodeTitle][direction] || []

    let selected, highlight
    if (highlighted[direction][nodeTitle]) { // unselect
      highlighted[direction][nodeTitle] = false
      selected = false
      highlight = highlights.unselected
    } else { // select
      highlighted[direction][nodeTitle] = true
      selected = true
      highlight = highlights.selected
    }

    highlightPoints(nodePoints, highlight)
    nodeRelations.forEach((related) => {
      const relatedNode = selectByKey('.node', related)
      const relationTitle = edgeRelation(nodeTitle, getNodeTitle(relatedNode), direction)
      const edgePoints = selectByKey('.edge', relationTitle).selectAll('path,polygon')

      highlightPoints(edgePoints, highlight)

      // avoid trying to process the same node multiple times
      if (highlighted[direction][related] != selected) {
        highlightTree(relatedNode, direction)
      }
    })
  }

  d3.select('#graph')
    .graphviz()
    .dot(dot)
    .scale(0.5)
    .fit(true)
    .logEvents(true)
    .render(() => {
      d3.selectAll('.node').on('click', function() {
        let event = d3.event
        event.preventDefault()
        event.stopPropagation()

        if (event.shiftKey) {
          highlightTree(d3.select(this), 'ascendants')
        } else {
          highlightTree(d3.select(this), 'descendants')
        }
      })

      // prevent zoom when double click nodes or edges
      d3.selectAll('.node,.edge').on('dblclick', function() {
        let event = d3.event
        event.preventDefault()
        event.stopPropagation()
      })

      d3.selectAll('.node,.edge').on('mouseover', function() {
        d3.select(this).style('cursor', 'pointer');
      })

      d3.selectAll('.node,.edge').on('mouseout', function() {
        d3.select(this).style('cursor', 'default');
      })
    })
}())
